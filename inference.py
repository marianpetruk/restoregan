import torch
import argparse
import cv2
import yaml

import numpy as np

import matplotlib.pyplot as plt

from albumentations import Compose, Normalize
from models.networks import get_nets
from pathlib import Path
import os

# cv2.setNumThreads(0)


norm = Compose([Normalize(
    mean=[0.5, 0.5, 0.5],
    std=[0.5, 0.5, 0.5],
)])

DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def main(args, config):
    print("Start inference")
    head_file, tail_file = os.path.split(args.image)
    # filename, ext = tail_file.split(".")
    tail_file_path = Path(tail_file)
    output_filename = tail_file_path.name[:-len(tail_file_path.suffix)] + "_restored" + tail_file_path.suffix

    print("Reading input image...")
    try:
        img = cv2.imread(args.image)
        assert img is not None
    except:
        print("Exception during reading. Maybe file not found.")
        exit()

    # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    print("Resizing...")
    img = cv2.resize(img, tuple(config['image_size']))

    print("Normalizing...")
    norm_img = norm(image=img)['image']
    norm_img_tensor = torch.from_numpy(np.transpose(norm_img, (2, 0, 1)).astype('float32'))

    norm_img_tensor = norm_img_tensor.unsqueeze(0)
    norm_img_tensor = norm_img_tensor.to(DEVICE)

    # plt.imshow(img)
    # plt.show()

    print("Loading model weights...")
    print(config['model'])
    netG, _ = get_nets(config['model'])

    checkpoint = torch.load(args.weights)
    print(checkpoint.keys())
    netG.load_state_dict(checkpoint['model'])
    netG.cuda()

    # print(norm_img_tensor.shape)

    print("Inferring ...")
    with torch.no_grad():
        output = netG(norm_img_tensor)

    output = output.squeeze()
    output = output.detach().cpu().float().numpy()
    output = (np.transpose(output, (1, 2, 0)) + 1) / 2.0 * 255.0
    output = output.astype('uint8')
    print("Finished postprocessing")

    # plt.imshow(output)
    # plt.show()
    # plt.imshow(np.hstack((img, output)))
    # plt.show()

    cv2.imwrite(filename=os.path.join(head_file, output_filename), img=output)
    print("Restored image is written @ " + os.path.join(head_file, output_filename))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str, required=True, help="Path to config file. e.g. config/deblur_solver_wsl.yaml")
    parser.add_argument("--weights", type=str, required=True, help="Path to weights file. e.g. trained_models/fpn_fpn_resnext_wsl_content0.5_feature0.006_adv0.001/best_fpn_fpn_resnext_wsl_content0.5_feature0.006_adv0.001.h5")
    parser.add_argument("--image", type=str, required=True, help="Path to image file")
    args = parser.parse_args()

    with open(args.config, 'r') as f:
        config = yaml.load(f)
    main(args, config)
